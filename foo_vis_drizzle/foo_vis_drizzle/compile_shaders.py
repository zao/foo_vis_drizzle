#!/usr/bin/env python3

from subprocess import call

fxc = "C:\\Program Files (x86)\\Windows Kits\\8.1\\bin\\x64\\fxc.exe"

def compile(source, target, model, entrypoint, *opts):
	cmd = [fxc, source, '/Fo', target, '/T', model, '/E', entrypoint]
	fixedopts = ['/O3', '/Zi', '/nologo']
	cmd.extend(fixedopts)
	ec = call(cmd)
	if ec != 0:
		raise SystemExit

compile('testVS.hlsl', 'Z:\\public_html\\fvd\\test.vs.cso', 'vs_5_0', 'main')
compile('testPS.hlsl', 'Z:\\public_html\\fvd\\test.ps.cso', 'ps_5_0', 'main')

compile('meshVS.hlsl', 'Z:\\public_html\\fvd\\mesh.vs.cso', 'vs_5_0', 'main')
compile('meshPS.hlsl', 'Z:\\public_html\\fvd\\mesh.ps.cso', 'ps_5_0', 'main')