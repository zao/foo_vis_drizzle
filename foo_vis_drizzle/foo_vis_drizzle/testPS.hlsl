struct PixelInputType {
	float4 position : SV_Position;
	float3 normal : Normal;
	float4 tint : Tint;
};

Texture2D<float2> spectrumTex : register(t1);
SamplerState spectrumSampler : register(s0);

float4 main(PixelInputType ps) : SV_Target {
	float3 n = normalize(ps.normal);
	float4 c = float4(n * 0.5f + 0.5f, 1.0f);
	float2 tx = spectrumTex.Sample(spectrumSampler, ps.position.xy);
	c.rgba = float4(1.0f, 1.0f, 1.0f, 1.0f);
	c.rgba *= ps.tint;
	// c.rgb = tx.rrr;
	return c;
}