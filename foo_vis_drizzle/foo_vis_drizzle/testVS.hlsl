struct VertexInputType {
	float4 position : Position;
	float3 normal : Normal;
	uint id : SV_InstanceID;
};

struct PixelInputType {
	float4 position : SV_Position;
	float3 normal : Normal;
	float4 tint : Tint;
};

cbuffer Matrices {
	float4x4 ProjectionTransform;
	float4x4 ViewTransform;
	float4x4 NormalTransform;
};

struct Instance {
	float4x4 WorldTransform;
	float4x4 NormalTransform;
	float4 Tint;
};

StructuredBuffer<Instance> instances : register(t0);
Texture2D<float2> spectrumTex : register(t1);
SamplerState spectrumSampler : register(s0);

PixelInputType main(VertexInputType vs) {
	PixelInputType ps;
	Instance inst = instances[vs.id];
	ps.position = mul(ProjectionTransform, mul(ViewTransform, mul(inst.WorldTransform, vs.position)));
	ps.normal = mul(NormalTransform, mul(inst.NormalTransform, float4(vs.normal, 0.0f))).xyz;
	ps.tint = inst.Tint;
	return ps;
}