#pragma once

#include <stdint.h>

struct float2 {
	float x, y;

	float const* Data() const {
		return (float const*)this;
	}

	float* Data() {
		return (float*)this;
	}
};

struct float3 {
	float x, y, z;

	float const* Data() const {
		return (float const*)this;
	}

	float* Data() {
		return (float*)this;
	}
};

struct float4 {
	float x, y, z, w;

	float const* Data() const {
		return (float const*)this;
	}

	float* Data() {
		return (float*)this;
	}
};