#include <ATLHelpers.h>
#include <atlframe.h>

#pragma comment(lib, "winmm.lib")

#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

#include <algorithm>
#include <atomic>
#include <iterator>
#include <mutex>
#include <random>
#include <memory>
#include <thread>
#include <vector>

#include <d3d11.h>
#include <dxgi.h>

#include <json/json.h>
#include <MathGeoLib.h>
#include "stb_image_write.h"

#include "VSink.h"

struct Particle {
	float3 position;
	float3 velocity;
	Quat orientationChange;
	Quat orientation;
	float age;
	float lifeLeft;
};

struct Spectrum {
	std::vector<float> values;
	int channels;
	int size;
	double t;
};

template <typename C>
void SimParticles(float dt, Spectrum const* spectrum, C& particles) {
	{
		auto& c = particles;
		auto I = std::remove_if(c.begin(), c.end(), [](auto& p) { return !p.lifeLeft; });
		c.erase(I, c.end());
	}
	for (size_t i = 0; i < 100; ++i) {
		static LCG instanceLCG(9001);
		Particle p{};
		p.age = 0.0f;
		p.lifeLeft = 1.0f;
		p.position = float3::RandomBox(instanceLCG, float3(-10.0f, 0.0f, 0.0f), float3(10.0f, 0.0f, 0.0f));
		if (spectrum) {
			float freq = Clamp01((p.position.x + 10.0f) / 20.0f);
			freq = sqrt(freq);
			auto n = spectrum->size;
			int idx = Clamp((int)(freq * n), 0, n - 1);
			size_t channel = 0;
			float v = spectrum->values[idx*spectrum->channels + channel];
			p.position.y += 10.0f * pow(v, 0.25f);
			//p.lifeLeft = sqrt(v) * 10.0f;
		}
		float3 dir = float3::unitZ;
		float fwness = instanceLCG.FloatIncl(0.9f, 1.1f);
		float upness = instanceLCG.Float(0.0f, 0.3f);
		float3 vel =
			float3::unitZ * fwness +
			float3::unitY * upness;
		p.velocity = vel;
		p.orientation = Quat::RandomRotation(instanceLCG);
		float3 axis = float3::RandomDir(instanceLCG);
		p.orientationChange = Quat(axis, pi / 4.0f);
		particles.push_back(std::move(p));
	}
}

template <typename C>
void StepParticles(float dt, C& particles) {
	//float3 const G(0.0f, -9.81f, 0.0f);
	//float3 const G = float3::zero;
	float3 const G = float3{ 0.0f, -9.81f, 0.0f };
	for (auto& p : particles) {
		if (!p.lifeLeft) continue;
		p.velocity += G * dt;
		p.position += p.velocity * dt;
		p.orientation = Slerp(Quat::identity, p.orientationChange, dt) * p.orientation;
		float rem = Min(p.lifeLeft, dt);
		p.age += rem;
		p.lifeLeft -= rem;
		if (p.lifeLeft < 0.0f) p.lifeLeft = 0.0f;
	}
}

static float4 FromRGBColorRefToFloat4(uint32_t c) {
	float4 ret = {
		((c>>0) & 0xFF) / 255.0f,
		((c>>8) & 0xFF) / 255.0f,
		((c>>16) & 0xFF) / 255.0f,
		1.0f
	};
	return ret;
}

static float4 FromRGBAColorRefToFloat4(uint32_t c) {
	float4 ret = {
		((c>>0) & 0xFF) / 255.0f,
		((c>>8) & 0xFF) / 255.0f,
		((c>>16) & 0xFF) / 255.0f,
		((c>>24) & 0xFF) / 255.0f
	};
	return ret;
}

struct WritePNGContext {
	std::vector<uint32_t> data;
	size_t width;
	size_t height;
	size_t pitch;
	std::string filename;
};

DWORD WINAPI WritePNG(void* param) {
	auto* ctx = reinterpret_cast<WritePNGContext*>(param);
	stbi_write_png(ctx->filename.c_str(), ctx->width, ctx->height, 4, ctx->data.data(), ctx->pitch);
	delete ctx;
	return 0;
}

// {D2EFD985-57EC-4A5B-AF60-51DEDA7D4A80}
static const GUID g_drizzleGUID =
{ 0xd2efd985, 0x57ec, 0x4a5b,{ 0xaf, 0x60, 0x51, 0xde, 0xda, 0x7d, 0x4a, 0x80 } };

static const GUID g_drizzleSubclassGUID = ui_element_subclass_playback_visualisation;

class WindowInstance : public ui_element_instance, public CWindowImpl<WindowInstance> {
	virtual void set_configuration(ui_element_config::ptr data) override {
	}

	virtual ui_element_config::ptr get_configuration() override {
		ui_element_config::ptr ret = ui_element_config::g_create_empty(g_drizzleGUID);
		return ret;
	}

	virtual bool edit_mode_context_menu_test(const POINT & point, bool isFromKeyboard) override { return true; }

public:
	void initialize_window(HWND parent) {
		this->Create(parent, 0, 0, WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS);
		renderThread = std::make_unique<std::thread>([this] { RenderMain(); });

		static_api_ptr_t<visualisation_manager> vm;
		vm->create_stream(visStream, visualisation_manager::KStreamFlagNewFFT);
		SetTimer(0x4040, 1, nullptr);
	}

	static GUID g_get_guid() {
		return g_drizzleGUID;
	}

	static GUID g_get_subclass() {
		return g_drizzleSubclassGUID;
	}

	static void g_get_name(pfc::string_base& out) {
		out.set_string("Drizzle Visualisation");
	}

	static ui_element_config::ptr g_get_default_configuration() {
		ui_element_config::ptr ret = ui_element_config::g_create_empty(g_get_guid());
		return ret;
	}

	static char const* g_get_description() {
		return "A soothing visualisation with falling particles.";
	}

	virtual void notify(GUID const& what, t_size param1, void const* param2, t_size param2Size) override {
		if (what == ui_element_notify_colors_changed) {
			if (fgPen) fgPen.DeleteObject();
			if (bgBrush) bgBrush.DeleteObject();
			Invalidate();
		}
	}

	ui_element_config::ptr conf;
	ui_element_instance_callback_ptr cb;

public:
	WindowInstance(ui_element_config::ptr conf, ui_element_instance_callback_ptr cb)
		: conf(conf)
		, cb(cb)
	{}

public:
	DECLARE_WND_CLASS_EX(L"foo_vis_drizzle", CS_VREDRAW | CS_HREDRAW | CS_OWNDC, 0)

	BEGIN_MSG_MAP_EX(WindowInstance)
	MSG_WM_DESTROY(OnWM_DESTROY);
	MSG_WM_ERASEBKGND(OnWM_ERASEBKGND);
	MSG_WM_RBUTTONUP(OnWM_RBUTTONUP);
	MSG_WM_SIZE(OnWM_SIZE);
	MSG_WM_TIMER(OnWM_TIMER);
	END_MSG_MAP()

	auto OnWM_DESTROY() -> void {
		KillTimer(0x4040);
		shutdownRenderFlag = true;
		renderThread->join();
	}

	auto OnWM_ERASEBKGND(CDCHandle dc) -> LRESULT {
		Invalidate(FALSE);
		return 1;
	}

	CPen fgPen;
	CBrush bgBrush;

	struct ViewBundle {
		CComPtr<IDXGISwapChain> swapChain;
		CComPtr<ID3D11Device> dev;
		CComPtr<ID3D11DeviceContext> ctx;
		D3D_FEATURE_LEVEL featureLevel;

		CComPtr<ID3D11Texture2D> rtT2;
		CComPtr<ID3D11Texture2D> ctT2;
		CComPtr<ID3D11Texture2D> stageT2;
		CComPtr<ID3D11RenderTargetView> bbv;
		CComPtr<ID3D11RenderTargetView> rtv;
		CComPtr<ID3D11Texture2D> ds;
		CComPtr<ID3D11DepthStencilView> dsv;
		CComPtr<ID3D11DepthStencilState> dss;
		CComPtr<ID3D11BlendState> bs;
		D3D11_VIEWPORT viewport;
	};
	ViewBundle vb;

	struct ShaderBundle {
		CComPtr<ID3D11VertexShader> vs;
		CComPtr<ID3D11PixelShader> ps;
		CComPtr<ID3D11InputLayout> il;
	};

	struct VSink {
		VSink() {
			module = LoadLibraryA("C:\\Code\\videosink\\vsink\\Debug\\vsink.dll");
			if (module) {
				CreateContext = (decltype(CreateContext))GetProcAddress(module, "vsink_ctx_create");
				DestroyContext = (decltype(DestroyContext))GetProcAddress(module, "vsink_ctx_destroy");
				CreateFilm = (decltype(CreateFilm))GetProcAddress(module, "vsink_film_create");
				DestroyFilm = (decltype(DestroyFilm))GetProcAddress(module, "vsink_film_destroy");
				AddFrameToFilm = (decltype(AddFrameToFilm))GetProcAddress(module, "vsink_film_add_frame");
			}
		}

		~VSink() {
			// FreeLibrary(module);
		}

		HMODULE module;
		VSINK_PROTO_CTX_CREATE(*CreateContext);
		VSINK_PROTO_CTX_DESTROY(*DestroyContext);
		VSINK_PROTO_FILM_CREATE(*CreateFilm);
		VSINK_PROTO_FILM_DESTROY(*DestroyFilm);
		VSINK_PROTO_FILM_ADD_FRAME(*AddFrameToFilm);
	};

	VSink* vsink;
	uint32_t cropWidth, cropHeight;
	vsink_ctx_t* vsContext;
	vsink_film_t* vsFilm;

	ShaderBundle particleProgram;
	ShaderBundle meshProgram;
	CComPtr<ID3D11Buffer> particleVB;
	int particleVertexCount;
	CComPtr<ID3D11Buffer> pillarVB;
	int pillarVertexCount;
	CComPtr<ID3D11Buffer> matrixCB;
	CComPtr<ID3D11Texture2D> spectrumT2;
	CComPtr<ID3D11ShaderResourceView> spectrumSRV;
	CComPtr<ID3D11SamplerState> spectrumSS;

	service_ptr_t<visualisation_stream_v2> visStream;

	struct Vertex {
		float3 pos;
		float3 normal;
	};

	struct MeshInstance {
		float4x4 vectorTransform;
		float4x4 bivectorTransform;
	};

	struct ParticleInstance {
		float4x4 vectorTransform;
		float4x4 bivectorTransform;
		float4 tint;
	};

	std::vector<Particle> particles;

	void Draw(std::vector<Particle>&& particles) {
		auto bgColor = cb->query_std_color(ui_color_background);
		auto fgColor = cb->query_std_color(ui_color_text);
		float4 clearColor = FromRGBColorRefToFloat4(bgColor);
		vb.ctx->RSSetViewports(1, &vb.viewport);
		vb.ctx->ClearRenderTargetView(vb.rtv, clearColor.ptr());
		float4 gray = float4{0.5f, 0.5f, 0.5f, 1.0f};
		vb.ctx->ClearRenderTargetView(vb.bbv, gray.ptr());
		vb.ctx->ClearDepthStencilView(vb.dsv, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
		vb.ctx->OMSetRenderTargets(1, &vb.rtv.p, vb.dsv);

		{
			std::vector<MeshInstance> instances;
#if 0
			{
				MeshInstance inst{};
				inst.vectorTransform = float4x4::identity;
				inst.bivectorTransform = float4x4::identity;
				instances.assign(&inst, &inst+1);
			}
#endif
			if (instances.size()) {
				size_t instanceCount = instances.size();
				CComPtr<ID3D11Buffer> instanceSB;
				CComPtr<ID3D11ShaderResourceView> instanceSRV;
				{
					D3D11_BUFFER_DESC bd = {};
					bd.ByteWidth = sizeof(MeshInstance) * instanceCount;
					bd.Usage = D3D11_USAGE_IMMUTABLE;
					bd.BindFlags = D3D11_BIND_SHADER_RESOURCE;
					bd.CPUAccessFlags = 0;
					bd.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
					bd.StructureByteStride = sizeof(MeshInstance);

					MeshInstance* p = instances.data();
					D3D11_SUBRESOURCE_DATA srd = {};
					srd.pSysMem = p;
					vb.dev->CreateBuffer(&bd, &srd, &instanceSB);
					vb.dev->CreateShaderResourceView(instanceSB, nullptr, &instanceSRV);
				}
				ID3D11Buffer* vbs[] = { pillarVB.p };
				UINT strides[] = { sizeof(Vertex) };
				UINT offsets[] = { 0 };
				vb.ctx->IASetVertexBuffers(0, 1, vbs, strides, offsets);
				vb.ctx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
				vb.ctx->IASetInputLayout(meshProgram.il);
				vb.ctx->VSSetShader(meshProgram.vs, nullptr, 0);
				vb.ctx->PSSetShader(meshProgram.ps, nullptr, 0);
				vb.ctx->VSSetConstantBuffers(0, 1, &matrixCB.p);
				vb.ctx->VSSetShaderResources(0, 1, &instanceSRV.p);
				vb.ctx->VSSetShaderResources(1, 1, &spectrumSRV.p);
				vb.ctx->VSSetSamplers(0, 1, &spectrumSS.p);
				vb.ctx->PSSetShaderResources(1, 1, &spectrumSRV.p);
				vb.ctx->PSSetSamplers(0, 1, &spectrumSS.p);

				vb.ctx->DrawInstanced(pillarVertexCount, instanceCount, 0, 0);

				{
					ID3D11Buffer* nullBuffers[] = { nullptr };
					ID3D11ShaderResourceView* nullResources[] = { nullptr };
					ID3D11SamplerState* nullSamplers[] = { nullptr };
					vb.ctx->IASetVertexBuffers(0, 1, nullBuffers, strides, offsets);
					vb.ctx->IASetInputLayout(nullptr);
					vb.ctx->VSSetShader(nullptr, nullptr, 0);
					vb.ctx->PSSetShader(nullptr, nullptr, 0);
					vb.ctx->VSSetConstantBuffers(0, 1, nullBuffers);
					vb.ctx->VSSetShaderResources(0, 1, nullResources);
					vb.ctx->VSSetShaderResources(1, 1, nullResources);
					vb.ctx->VSSetSamplers(0, 1, nullSamplers);
					vb.ctx->PSSetShaderResources(1, 1, nullResources);
					vb.ctx->PSSetSamplers(0, 1, nullSamplers);
				}
			}
		}

		{
			std::vector<ParticleInstance> instances;
			instances.clear();
			for (auto& p : particles) {
				ParticleInstance inst{};
				float4x4 M = float4x4::Translate(p.position).ToFloat4x4() * p.orientationChange;
				inst.vectorTransform = M.Transposed();
				inst.bivectorTransform = M.InverseTransposed().Transposed();
				{
					float den = p.age + p.lifeLeft;
					float f = den ? p.age / den : 1.0f;
					inst.tint = Lerp(float4(0.9f, 0.8f, 0.7f, 1.0f), float4(0.3f, 0.3f, 0.3f, 0.3f), f);
				}
				instances.push_back(std::move(inst));
			}

			if (instances.size()) {
				std::sort(instances.begin(), instances.end(), [](auto&& lhs, auto&& rhs) {
					float4x4 const& a = lhs.vectorTransform;
					float4x4 const& b = rhs.vectorTransform;
					return a.TranslatePart()[2] > b.TranslatePart()[2];
				});
				size_t instanceCount = instances.size();
				CComPtr<ID3D11Buffer> instanceSB;
				CComPtr<ID3D11ShaderResourceView> instanceSRV;
				{
					DWORD t = timeGetTime();
					D3D11_BUFFER_DESC bd = {};
					bd.ByteWidth = sizeof(ParticleInstance) * instanceCount;
					bd.Usage = D3D11_USAGE_IMMUTABLE;
					bd.BindFlags = D3D11_BIND_SHADER_RESOURCE;
					bd.CPUAccessFlags = 0;
					bd.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
					bd.StructureByteStride = sizeof(ParticleInstance);

					ParticleInstance* p = instances.data();
					D3D11_SUBRESOURCE_DATA srd = {};
					srd.pSysMem = p;
					vb.dev->CreateBuffer(&bd, &srd, &instanceSB);
					vb.dev->CreateShaderResourceView(instanceSB, nullptr, &instanceSRV);
				}
				ID3D11Buffer* vbs[] = { particleVB.p };
				UINT strides[] = { sizeof(Vertex) };
				UINT offsets[] = { 0 };
				vb.ctx->IASetVertexBuffers(0, 1, vbs, strides, offsets);
				vb.ctx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
				vb.ctx->IASetInputLayout(particleProgram.il);
				vb.ctx->VSSetShader(particleProgram.vs, nullptr, 0);
				vb.ctx->PSSetShader(particleProgram.ps, nullptr, 0);
				vb.ctx->VSSetConstantBuffers(0, 1, &matrixCB.p);
				vb.ctx->VSSetShaderResources(0, 1, &instanceSRV.p);
				vb.ctx->VSSetShaderResources(1, 1, &spectrumSRV.p);
				vb.ctx->VSSetSamplers(0, 1, &spectrumSS.p);
				vb.ctx->PSSetShaderResources(1, 1, &spectrumSRV.p);
				vb.ctx->PSSetSamplers(0, 1, &spectrumSS.p);

				vb.ctx->DrawInstanced(particleVertexCount, instanceCount, 0, 0);

				{
					ID3D11Buffer* nullBuffers[] = { nullptr };
					ID3D11ShaderResourceView* nullResources[] = { nullptr };
					ID3D11SamplerState* nullSamplers[] = { nullptr };
					vb.ctx->IASetVertexBuffers(0, 1, nullBuffers, strides, offsets);
					vb.ctx->IASetInputLayout(nullptr);
					vb.ctx->VSSetShader(nullptr, nullptr, 0);
					vb.ctx->PSSetShader(nullptr, nullptr, 0);
					vb.ctx->VSSetConstantBuffers(0, 1, nullBuffers);
					vb.ctx->VSSetShaderResources(0, 1, nullResources);
					vb.ctx->VSSetShaderResources(1, 1, nullResources);
					vb.ctx->VSSetSamplers(0, 1, nullSamplers);
					vb.ctx->PSSetShaderResources(1, 1, nullResources);
					vb.ctx->PSSetSamplers(0, 1, nullSamplers);
				}
			}
		}

		CComPtr<ID3D11Resource> bb;
		vb.bbv->GetResource(&bb);
		vb.ctx->CopyResource(bb, vb.rtT2);
		vb.swapChain->Present(0, 0);

		D3D11_TEXTURE2D_DESC t2d = {};
		vb.rtT2->GetDesc(&t2d);
		vb.ctx->ResolveSubresource(vb.ctT2, 0, vb.rtT2, 0, t2d.Format);
		vb.ctx->CopyResource(vb.stageT2, vb.ctT2);
		vb.ctx->OMSetRenderTargets(1, &vb.bbv.p, vb.dsv);
		D3D11_MAPPED_SUBRESOURCE msr = {};
		if (SUCCEEDED(vb.ctx->Map(vb.stageT2, 0, D3D11_MAP_READ, 0, &msr))) {
			size_t const CropWidth = cropWidth;
			size_t const CropHeight = cropHeight;
			size_t const RowSize = CropWidth * sizeof(uint32_t);
			std::vector<uint32_t> v(RowSize * CropHeight);
			char* p = (char*)msr.pData;
			char* q = (char*)v.data();
			for (size_t row = 0; row < CropHeight; ++row) {
				memcpy(q, p, RowSize);
				p += msr.RowPitch;
				q += RowSize;
			}
			vb.ctx->Unmap(vb.stageT2, 0);
			if (0) {
				std::wostringstream oss;
				oss << "D:\\Temp\\foo-" << CropWidth << "x" << CropHeight << L".data";
				HANDLE h = CreateFile(oss.str().c_str(), GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS, 0, nullptr);
				DWORD nWritten = 0;
				WriteFile(h, v.data(), v.size(), &nWritten, nullptr);
				CloseHandle(h);
			}
			if (0 && vsink) {
				vsink->AddFrameToFilm(vsFilm, v.data(), (uint16_t)RowSize);
			}
			if (0) {
				static uint64_t i = 0;
				char buf[MAX_PATH] = {};
				sprintf(buf, "D:\\Temp\\fvd\\capture-%06d.png", i);
				WritePNGContext* ctx = new WritePNGContext();
				ctx->data = std::move(v);
				ctx->width = CropWidth;
				ctx->height = CropHeight;
				ctx->pitch = RowSize;
				ctx->filename = buf;
				QueueUserWorkItem(&WritePNG, ctx, WT_EXECUTEDEFAULT);
				++i;
			}
		}
	}

	auto OnWM_RBUTTONUP(UINT flags, CPoint point) -> void {
		if (cb->is_edit_mode_enabled()) {
			SetMsgHandled(FALSE);
		}
		else {
		}
	}

	void ReleaseResolutionIndependentResources() {
		particleProgram = {};
		meshProgram = {};
		pillarVB.Release();
		particleVB.Release();
		spectrumT2.Release();
		spectrumSRV.Release();
		spectrumSS.Release();
		delete vsink;
	}

	void CreateResolutionIndependentResources() {
		pfc::array_t<char> particleVsArr, particlePsArr, meshVsArr, meshPsArr;
		try {
			auto const GetShaderSource = [](char const* url, auto& arr) {
				static_api_ptr_t<http_client> hc;
				abort_callback_dummy acb;
				auto req = hc->create_request("GET");
				auto fh = req->run(url, acb);
				fh->read_till_eof(arr, acb);
			};
			GetShaderSource("http://zao.se/~zao/fvd/test.vs.cso", particleVsArr);
			GetShaderSource("http://zao.se/~zao/fvd/test.ps.cso", particlePsArr);
			GetShaderSource("http://zao.se/~zao/fvd/mesh.vs.cso", meshVsArr);
			GetShaderSource("http://zao.se/~zao/fvd/mesh.ps.cso", meshPsArr);
		}
		catch (pfc::exception&) {
			return;
		}
		auto const CreateProgram = [&dev = vb.dev](auto const& vsSource, auto const& psSource, auto const& ieds, auto& dst) {
			dev->CreateVertexShader(vsSource.get_ptr(), vsSource.get_count(), nullptr, &dst.vs);
			dev->CreatePixelShader(psSource.get_ptr(), psSource.get_count(), nullptr, &dst.ps);
			dev->CreateInputLayout(ieds.data(), ieds.size(), vsSource.get_ptr(), vsSource.get_count(), &dst.il);
		};
		std::vector<D3D11_INPUT_ELEMENT_DESC> particleElems = {
			D3D11_INPUT_ELEMENT_DESC{"Position", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
			D3D11_INPUT_ELEMENT_DESC{"Normal", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0}
		};
		std::vector<D3D11_INPUT_ELEMENT_DESC> meshElems = {
			D3D11_INPUT_ELEMENT_DESC{ "Position", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			D3D11_INPUT_ELEMENT_DESC{ "Normal", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};
		CreateProgram(particleVsArr, particlePsArr, particleElems, particleProgram);
		CreateProgram(meshVsArr, meshPsArr, meshElems, meshProgram);

		// NOTE(zao): Construct particle mesh
		{
			float3 normals[] = { float3{0.0f, 0.0f, 1.0f}, float3{0.0f, 0.0f, -1.0f} };
			int const CornerCount = 4;
			float3 corners[CornerCount] = {};
			float3 baseVec = float3::unitX * 0.02f;
			for (int i = 0; i < CornerCount; ++i) {
				float3x3 R = float3x3::RotateZ(i*(2*pi/CornerCount));
				corners[i] = R*baseVec;
			}
			UINT const SliceCount = CornerCount - 2;
			UINT const VertexCount = SliceCount*6;
			particleVertexCount = VertexCount;
			Vertex vertices[VertexCount] = {};
			{
				Vertex* vp = vertices;
				for (int i = 0; i < SliceCount; ++i) {
					int j = (i+1)%CornerCount;
					*vp++ = Vertex{ corners[0], normals[0] };
					*vp++ = Vertex{ corners[i], normals[0] };
					*vp++ = Vertex{ corners[j], normals[0] };
					*vp++ = Vertex{ corners[0], normals[1] };
					*vp++ = Vertex{ corners[j], normals[1] };
					*vp++ = Vertex{ corners[i], normals[1] };
				}
			}
			assert(VertexCount == sizeof(vertices)/sizeof(vertices[0]));
			D3D11_BUFFER_DESC bd = {};
			bd.ByteWidth = sizeof(vertices);
			bd.Usage = D3D11_USAGE_IMMUTABLE;
			bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			bd.CPUAccessFlags = 0;
			bd.MiscFlags = 0;
			bd.StructureByteStride = 0;
			D3D11_SUBRESOURCE_DATA srd = {};
			srd.pSysMem = vertices;
			srd.SysMemPitch = sizeof(Vertex);
			srd.SysMemSlicePitch = sizeof(vertices);
			vb.dev->CreateBuffer(&bd, &srd, &particleVB);
		}

		// NOTE(zao): Construct pillar mesh
		{
			float3 normals[] = { float3{ 0.0f, 0.0f, 1.0f } };
			float3 corners[] = {
				float3{ -0.3f, +10.0f, 0.0f },
				float3{ +0.3f, +10.0f, 0.0f },
				float3{ +0.3f, -10.0f, 0.0f },
				float3{ -0.3f, -10.0f, 0.0f }
			};
			Vertex vertices[] = {
				Vertex{ corners[0], normals[0] }, Vertex{ corners[1], normals[0] }, Vertex{ corners[2], normals[0] },
				Vertex{ corners[0], normals[0] }, Vertex{ corners[2], normals[0] }, Vertex{ corners[3], normals[0] }
			};
			UINT const VertexCount = 6;
			pillarVertexCount = VertexCount;
			D3D11_BUFFER_DESC bd = {};
			bd.ByteWidth = sizeof(vertices);
			bd.Usage = D3D11_USAGE_IMMUTABLE;
			bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			bd.CPUAccessFlags = 0;
			bd.MiscFlags = 0;
			bd.StructureByteStride = 0;
			D3D11_SUBRESOURCE_DATA srd = {};
			srd.pSysMem = vertices;
			srd.SysMemPitch = sizeof(Vertex);
			srd.SysMemSlicePitch = sizeof(vertices);
			vb.dev->CreateBuffer(&bd, &srd, &pillarVB);
		}

		// NOTE(zao): Prepare spectrum texture
		{
			UINT const SpectrumWidth = 1024;
			UINT const SpectrumHistoryCount = 128;
			D3D11_TEXTURE2D_DESC td = {};
			td.Width = SpectrumWidth;
			td.Height = SpectrumHistoryCount;
			td.MipLevels = 1;
			td.ArraySize = 1;
			td.Format = DXGI_FORMAT_R32G32_FLOAT;
			td.SampleDesc.Count = 1;
			td.SampleDesc.Quality = 0;
			td.Usage = D3D11_USAGE_DYNAMIC;
			td.BindFlags = D3D11_BIND_SHADER_RESOURCE;
			td.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			td.MiscFlags = 0;
			std::vector<float2> buf(SpectrumWidth * SpectrumHistoryCount);
			std::mt19937 rng(9001);
			std::uniform_real<float> dist(0.0f, 1.0f);
			std::generate(buf.begin(), buf.end(), [&rng,&dist]{
				float2 ret; ret.x = dist(rng); ret.y = dist(rng); return ret;
			});
			D3D11_SUBRESOURCE_DATA srd = {};
			srd.pSysMem = buf.data();
			srd.SysMemPitch = SpectrumWidth * sizeof(float2);
			vb.dev->CreateTexture2D(&td, &srd, &spectrumT2);
			vb.dev->CreateShaderResourceView(spectrumT2, nullptr, &spectrumSRV);

			CD3D11_SAMPLER_DESC sd{CD3D11_DEFAULT{}};
			vb.dev->CreateSamplerState(&sd, &spectrumSS);
		}
		vsink = new VSink();
		if (!vsink->module) {
			delete vsink;
		}
	}

	std::atomic<bool> shutdownRenderFlag{false};
	std::unique_ptr<std::thread> renderThread;

	void RenderMain() {
		CRect r;
		GetClientRect(&r);
		DXGI_SWAP_CHAIN_DESC scd = {};
		scd.BufferDesc.Width = r.Width() ? r.Width() : 8;
		scd.BufferDesc.Height = r.Height() ? r.Height() : 8;
		scd.BufferDesc.RefreshRate.Numerator = 0;
		scd.BufferDesc.RefreshRate.Denominator = 1;
		scd.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
		scd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		scd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
		scd.SampleDesc.Count = 8;
		scd.SampleDesc.Quality = 0;
		scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		scd.BufferCount = 1;
		scd.OutputWindow = *this;
		scd.Windowed = TRUE;
		scd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
		scd.Flags = 0;

		DWORD baseCreateFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;
		DWORD optionalFlags[] = { D3D11_CREATE_DEVICE_DEBUG, 0 };
		for (auto opts : optionalFlags) {
			DWORD createFlags = baseCreateFlags | opts;
			HRESULT hr = D3D11CreateDeviceAndSwapChain(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, createFlags, nullptr, 0,
				D3D11_SDK_VERSION, &scd, &vb.swapChain, &vb.dev, &vb.featureLevel, &vb.ctx);
			if (SUCCEEDED(hr)) break;
		}

		CRect clientRect;
		GetWindowRect(clientRect);
		CreateResolutionIndependentResources();
		CreateResolutionDependentResources();

		std::vector<Particle> renderParticles;
		DWORD const MDT = 1000/120;
		DWORD tic = timeGetTime();
		timeBeginPeriod(1);

		while (!shutdownRenderFlag) {
			DWORD toc = timeGetTime();
			while (toc - tic >= MDT) {
				decltype(spectrumData) spectrum;
				spectrum = std::atomic_load(&spectrumData);
				SimParticles(MDT / 1000.0f, spectrum.get(), particles);
				StepParticles(MDT / 1000.0f, particles);
				renderParticles = particles;
				tic += MDT;
			}
			CRect currentClientRect;
			GetClientRect(currentClientRect);
			if (currentClientRect.bottom != clientRect.bottom || currentClientRect.right != clientRect.right) {
				vb.ctx->ClearState();
				ReleaseResolutionDependentResources();
				vb.swapChain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, 0);
				CreateResolutionDependentResources();
				clientRect = currentClientRect;
			}
			Draw(std::move(renderParticles));
		}
		timeEndPeriod(1);
		ReleaseResolutionIndependentResources();
		ReleaseResolutionDependentResources();
		vb = ViewBundle{};
	}

	void ReleaseResolutionDependentResources() {
		if (vsink) {
			vsink->DestroyFilm(vsFilm);
			vsink->DestroyContext(vsContext);
		}
		vb.stageT2.Release();
		vb.rtT2.Release();
		vb.ctT2.Release();
		vb.rtv.Release();
		vb.bbv.Release();
		vb.ds.Release();
		vb.dsv.Release();
		memset(&vb.viewport, 0, sizeof(D3D11_VIEWPORT));
		matrixCB.Release();
	}

	void CreateResolutionDependentResources() {
		CRect r;
		GetClientRect(&r);
		CComPtr<ID3D11Texture2D> backBuffer;
		vb.swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&backBuffer);
		vb.dev->CreateRenderTargetView(backBuffer, nullptr, &vb.bbv);

		D3D11_TEXTURE2D_DESC rtd = {};
		rtd.Width = r.Width();
		rtd.Height = r.Height();
		rtd.MipLevels = 1;
		rtd.ArraySize = 1;
		rtd.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
		rtd.SampleDesc.Count = 8;
		rtd.SampleDesc.Quality = 0;
		rtd.Usage = D3D11_USAGE_DEFAULT;
		rtd.BindFlags = D3D11_BIND_RENDER_TARGET;
		rtd.CPUAccessFlags = 0;
		rtd.MiscFlags = 0;
		vb.dev->CreateTexture2D(&rtd, nullptr, &vb.rtT2);
		vb.dev->CreateRenderTargetView(vb.rtT2, nullptr, &vb.rtv);

		D3D11_TEXTURE2D_DESC ctd = rtd;
		ctd.Usage = D3D11_USAGE_DEFAULT;
		ctd.BindFlags = 0;
		ctd.SampleDesc.Count = 1;
		ctd.SampleDesc.Quality = 0;
		ctd.CPUAccessFlags = 0;
		vb.dev->CreateTexture2D(&ctd, nullptr, &vb.ctT2);

		D3D11_TEXTURE2D_DESC stagetd = ctd;
		stagetd.Usage = D3D11_USAGE_STAGING;
		stagetd.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
		vb.dev->CreateTexture2D(&stagetd, nullptr, &vb.stageT2);

		D3D11_TEXTURE2D_DESC dsd = {};
		dsd.Width = r.Width();
		dsd.Height = r.Height();
		dsd.MipLevels = 1;
		dsd.ArraySize = 1;
		dsd.Format = DXGI_FORMAT_D32_FLOAT;
		dsd.SampleDesc.Count = 8;
		dsd.SampleDesc.Quality = 0;
		dsd.Usage = D3D11_USAGE_DEFAULT;
		dsd.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		dsd.CPUAccessFlags = 0;
		dsd.MiscFlags = 0;
		vb.dev->CreateTexture2D(&dsd, nullptr, &vb.ds);
		vb.dev->CreateDepthStencilView(vb.ds, nullptr, &vb.dsv);

		memset(&vb.viewport, 0, sizeof(D3D11_VIEWPORT));
		vb.viewport.Width = (float)r.Width();
		vb.viewport.Height = (float)r.Height();

		Frustum frustum;
		frustum.SetKind(FrustumSpaceD3D, FrustumRightHanded);
		float const AspectRatio = (float)r.Width() / (float)r.Height();
		frustum.SetVerticalFovAndAspectRatio(1.0f, AspectRatio);
		frustum.SetViewPlaneDistances(0.1f, 250.0f);
		{
			float3 eye{0.0f, -5.0f, 25.0f};
			float3 at{0.0f, -5.0f, 0.0f};
			float3x4 Camera = float3x4::LookAt(eye, at, -float3::unitZ, float3::unitY, float3::unitY);
			frustum.SetWorldMatrix(Camera);
		}
		{
			D3D11_BUFFER_DESC bd = {};
			bd.ByteWidth = sizeof(float4x4) * 3;
			bd.Usage = D3D11_USAGE_DYNAMIC;
			bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			bd.MiscFlags = 0;
			bd.StructureByteStride = 0;
			float4x4 viewMatrix = frustum.ViewMatrix();
			float4x4 matrices[] = {
				frustum.ProjectionMatrix().Transposed(),
				viewMatrix.Transposed(),
				viewMatrix.InverseTransposed().Transposed() };
			D3D11_SUBRESOURCE_DATA srd = {};
			srd.pSysMem = matrices;
			vb.dev->CreateBuffer(&bd, &srd, &matrixCB);
		}

		cropWidth = r.Width() & ~1;
		cropHeight = r.Height() & ~1;
		if (vsink) {
			vsContext = vsink->CreateContext();
			vsFilm = vsink->CreateFilm(vsContext, "C:\\Temp\\foo.mkv", cropWidth, cropHeight, 1, 60);
		}
	}

	auto OnWM_SIZE(UINT type, CSize size) -> void {
	}

	std::shared_ptr<Spectrum> spectrumData;

	auto OnWM_TIMER(UINT_PTR id) -> void {
		if (id == 0x4040) {
			std::shared_ptr<Spectrum> spectrum;
			if (visStream.is_valid()) {
				double now = 0.0;
				if (visStream->get_absolute_time(now)) {
					audio_chunk_fast_impl chunk;
					if (visStream->get_spectrum_absolute(chunk, now, 4096)) {
						spectrum = std::make_shared<Spectrum>();
						spectrum->channels = chunk.get_channel_count();
						spectrum->size = chunk.get_sample_count();
						spectrum->t = now;
						auto* p = chunk.get_data();
						spectrum->values.assign(p, p + chunk.get_data_size());
					}
				}
			}
			std::atomic_store(&spectrumData, spectrum);
		}
	}
};

class WindowElement : public ui_element_v2 {
};

static service_factory_t<ui_element_impl<WindowInstance>> g_windowElementFactory;

DECLARE_COMPONENT_VERSION("Drizzle Visualisation", "1.0", "zao")
VALIDATE_COMPONENT_FILENAME("foo_vis_drizzle.dll")