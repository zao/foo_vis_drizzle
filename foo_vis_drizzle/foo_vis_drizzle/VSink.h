#pragma once

// Generic helper definitions for shared library support
#if defined _WIN32 || defined __CYGWIN__
#define VSINK_HELPER_DLL_IMPORT __declspec(dllimport)
#define VSINK_HELPER_DLL_EXPORT __declspec(dllexport)
#define VSINK_HELPER_DLL_LOCAL
#else
#if __GNUC__ >= 4
#define VSINK_HELPER_DLL_IMPORT __attribute__ ((visibility ("default")))
#define VSINK_HELPER_DLL_EXPORT __attribute__ ((visibility ("default")))
#define VSINK_HELPER_DLL_LOCAL  __attribute__ ((visibility ("hidden")))
#else
#define VSINK_HELPER_DLL_IMPORT
#define VSINK_HELPER_DLL_EXPORT
#define VSINK_HELPER_DLL_LOCAL
#endif
#endif

// Now we use the generic helper definitions above to define VSINK_API and VSINK_LOCAL.
// VSINK_API is used for the public API symbols. It either DLL imports or DLL exports (or does nothing for static build)
// VSINK_LOCAL is used for non-api symbols.

#ifdef VSINK_DLL // defined if FOX is compiled as a DLL
#ifdef VSINK_DLL_EXPORTS // defined if we are building the FOX DLL (instead of using it)
#define VSINK_API VSINK_HELPER_DLL_EXPORT
#else
#define VSINK_API VSINK_HELPER_DLL_IMPORT
#endif // VSINK_DLL_EXPORTS
#define VSINK_LOCAL VSINK_HELPER_DLL_LOCAL
#else // VSINK_DLL is not defined: this means FOX is a static lib.
#define VSINK_API
#define VSINK_LOCAL
#endif // VSINK_DLL

#ifndef VSINK_NOINCLUDE
#include <stdint.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

typedef struct vsink_ctx_s vsink_ctx_t;
typedef struct vsink_film_s vsink_film_t;

#define VSINK_PROTO_CTX_CREATE(declarator_name) VSINK_API \
vsink_ctx_t* (declarator_name)(void)
VSINK_PROTO_CTX_CREATE(vsink_ctx_create);

#define VSINK_PROTO_CTX_DESTROY(declarator_name) VSINK_API \
void (declarator_name)(vsink_ctx_t* ctx)
VSINK_PROTO_CTX_DESTROY(vsink_ctx_destroy);

#define VSINK_PROTO_FILM_CREATE(declarator_name) VSINK_API \
vsink_film_t* (declarator_name)(vsink_ctx_t* ctx, char const* filename, uint16_t width, uint16_t height, uint32_t time_num, uint32_t time_den)
VSINK_PROTO_FILM_CREATE(vsink_film_create);

#define VSINK_PROTO_FILM_DESTROY(declarator_name) VSINK_API \
void (declarator_name)(vsink_film_t* film)
VSINK_PROTO_FILM_DESTROY(vsink_film_destroy);

#define VSINK_PROTO_FILM_ADD_FRAME(declarator_name) VSINK_API \
void (declarator_name)(vsink_film_t* film, void* data, uint16_t pitch)
VSINK_PROTO_FILM_ADD_FRAME(vsink_film_add_frame);

#ifdef __cplusplus
}
#endif